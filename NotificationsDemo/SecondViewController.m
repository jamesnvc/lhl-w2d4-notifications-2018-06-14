//
//  SecondViewController.m
//  NotificationsDemo
//
//  Created by James Cash on 14-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController () <UITextFieldDelegate>

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.demoLabelDontDoThis.text = self.currentTimeLabelText;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"New text = %@, range = %@, replacement = %@", newText, NSStringFromRange(range), string);

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"TextChangeNotification"
     object:self
     userInfo:@{@"newText": newText}];

    return YES;
}

@end
