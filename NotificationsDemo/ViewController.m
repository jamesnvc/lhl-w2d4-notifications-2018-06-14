//
//  ViewController.m
//  NotificationsDemo
//
//  Created by James Cash on 14-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
// for segue demo
#import "SecondViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *theLabel;
@property (strong, nonatomic) IBOutlet UIButton *transitionButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleTextChangeNotification:)
     name:@"TextChangeNotification"
     object:nil];

    [self.transitionButton addTarget:self action:@selector(pressedDownButton:) forControlEvents:UIControlEventTouchDown];
}

- (void)pressedDownButton:(UIButton*)sender
{
    NSLog(@"Touchdown!🏈");
}

- (void)handleTextChangeNotification:(NSNotification*)notification
{
    NSLog(@"Got notification %@", notification);
    self.theLabel.text = notification.userInfo[@"newText"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doSomething:(id)sender {
    NSLog(@"Button touch up inside");
}
- (IBAction)somethingElse:(id)sender {
    NSLog(@"also touch up inside");
}

// this is not about notification center, this is just to demonstrate how to pass data in a segue (and how *not* to)
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"firstSegue"]) {
        SecondViewController *svc = segue.destinationViewController;
        // get current time as a string
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateStyle = NSDateFormatterNoStyle;
        fmt.timeStyle = NSDateFormatterLongStyle;
        NSString *now = [fmt stringFromDate:[NSDate date]];
        // set the label
        // WRONG WAY
        // this doesn't work due to how the view controller lifecycle works
        // at this point, the second viewcontroller has been init'd but the view hasn't been loaded yet!
        // because it hasn't been put on the screen, the view isn't loaded, so the label in the view hasn't been created, so the outlet isn't wired up
        svc.demoLabelDontDoThis.text = now;
        // the correct solution is to set a plain (i.e. non-view) property (in this case, we just want an NSString) of the SVC and then *it* will set the label in its viewDidLoad
        svc.currentTimeLabelText = now;
    }

}

@end
