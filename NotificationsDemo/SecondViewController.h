//
//  SecondViewController.h
//  NotificationsDemo
//
//  Created by James Cash on 14-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController
// normally outlets should be in the category-extension thing in the .m, we're just putting this here so the "doing it the wrong way demo works"
@property (strong, nonatomic) IBOutlet UILabel *demoLabelDontDoThis;

@property (strong, nonatomic) NSString* currentTimeLabelText;

@end
